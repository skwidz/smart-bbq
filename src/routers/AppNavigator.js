import React from 'react'

import { Platform } from 'react-native'
import { Root, StyleProvider } from 'native-base'
import { StackNavigator } from 'react-navigation'

import getTheme from '../../native-base-theme/components'
import material from '../../native-base-theme/variables/material'


import Home from '../components/home'
import Settings from '../containers/Settings'

const AppNavigator = StackNavigator(
	{
		Home: { screen: Home },
		Settings: { screen: Settings }
	},
	{
		initialRouteName: 'Home',
		headerMode: 'none'
	}
)

export default () =>
	(<Root>
		<StyleProvider style={getTheme(material)}>
			<AppNavigator />
		</StyleProvider>
	</Root>)
