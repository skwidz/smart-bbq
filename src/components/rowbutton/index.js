import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
	View,
	Text,
	Image,
	TouchableHighlight
} from 'react-native'
import { connect } from 'react-redux'
import {styles} from './styles'


class RowButton extends Component {
	static propTypes = {
	 	callback: PropTypes.func.isRequired,
	 	title: PropTypes.string.isRequired,
	 	subtitle: PropTypes.string,
	 	imageSource: PropTypes.func,
	 	showRight: PropTypes.bool,
	 	rightText: PropTypes.string,
	 	rightTextStyle: PropTypes.object,
	 	imageStyle: PropTypes.object,
	 	rowStyle: PropTypes.object,
	 	leftCheckEnabled: PropTypes.bool,
	 	rightCheckEnabled: PropTypes.bool,
	 	isChecked: PropTypes.bool
	}

	render() {
		return (
			<TouchableHighlight
				style={this.props.rowStyle
					? this.props.rowStyle
					: styles.row
				}
				onPress={() => this.props.callback()}
				underlayColor={'rgba(0,0,0,0.2)'}
			>
				<View style={{ flexDirection: 'row' }}>
					{this.props.leftCheckEnabled &&
						this.props.isChecked
						? <Text style={styles.text}>{'\u2714'}</Text>
						: null
					}
					<Image
						source={this.props.imageSource}
						style={this.props.imageStyle}
						resizeMode= 'contain'
					/>
					<View style={{
						flexDirection: 'column',
						paddingBottom: 10, paddingTop: 10,
						justifyContent: 'center'
					}}>

						<Text style={styles.text}>{this.props.title}</Text>
						{this.props.subtitle && <Text style={styles.subtitle}>{this.props.subtitle}</Text>}
					</View>
					{this.props.showRight != false
						? <Text style={this.props.rightTextStyle
							? this.props.rightTextStyle
							: styles.textArrow
						}>
							{this.props.rightText
								? this.props.rightText
								: '>'
							}
						</Text>
						: null
					}
					{this.props.rightCheckEnabled &&
						this.props.isChecked
						? <Text style={styles.text}>{'\u2714'}</Text>
						: null
					}

				</View>
			</TouchableHighlight>
		)
	}
}

export default connect()(RowButton)
