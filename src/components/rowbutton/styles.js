import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
	row: {
		backgroundColor: 'rgba(0,0,0,0.3)',
		marginBottom: 1
	},
	text: {
		color: 'white',
		fontSize: 15,
		paddingLeft: 15,
		alignSelf: 'flex-start'

	},
	subtitle: {
		color: 'rgba(255,255,255,0.6)',
		fontSize: 10,
		paddingLeft: 15,
		paddingTop: 5

	},
	textArrow: {
		color: 'white',
		fontSize: 20,
		paddingBottom: 10,
		paddingTop: 10,
		paddingLeft: 10,
		textAlign: 'right',
		alignSelf: 'center',
		flex: 2,
		paddingRight: 20
	}
})
