import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
	View,
	FlatList
} from 'react-native'
import TempCircle from './TempCircle'
import TimeCircle from './TimeCircle'


class CircleProgress extends Component {
	static propTypes = {
		data: PropTypes.array

	}

	constructor(props) {
		super(props)
		this.state = {
			startAngle: Math.PI * 1.5,
			angleLength: Math.PI * 2
		}
	}

	onPressItem() {

	}

	itemRenderer(item) {
		console.log(item)
		switch (item.type) {
		case 'timer':
			return (<TimeCircle fill={item.fill}/>)
		case 'temp':
			return (<TempCircle fill={item.fill}/>)
		default:
			return (<View/>)
		}
	}


	render() {
		return (
			<View>
				<FlatList
					horizontal={true}
					initialNumberToRender={3}
					data={this.props.data}
					keyExtractor={(item, index) => index}
					renderItem={(item) => this.itemRenderer(item.item)}
					ListHeaderComponent={<View style={{ width: 25 }}/>}
					ItemSeparatorComponent={() => <View style={{ width: 30 }}/>}
					ListFooterComponent={<View style={{ width: 25 }}/>}
				/>

			</View>
		)
	}
}


export default connect()(CircleProgress)
