import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
	View,
	Text
} from 'react-native'
import { AnimatedCircularProgress } from 'react-native-circular-progress'
import { styles } from './styles'

class TimeCircle extends Component {
	static propTypes = {
		fill: PropTypes.number.isRequired
	}

	constructor(props) {
		super(props)
		this.state = {
		}
	}

	render() {
		const {fill} = this.props 
		return (
			<AnimatedCircularProgress
				  size={120}
				  width={15}
				  fill={fill}
				  tintColor='#00e0ff'
				  onAnimationComplete={() => console.log('onAnimationComplete')}
				  backgroundColor='#3d5875'
				  arcSweepAngle={360}
				  rotation={0}
			>
				{(fill)=>(
					<Text style={styles.circleText}>{Math.round(fill)} min</Text>
				)}
			</AnimatedCircularProgress>
		)
	}
}


export default connect()(TimeCircle)
