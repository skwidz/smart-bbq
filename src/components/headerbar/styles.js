import { StyleSheet } from 'react-native'
import * as colors from '../../constants/Colors'


export const styles = StyleSheet.create({
	headerContainer: {
		height: 45,
		flexDirection: 'row',
		alignItems: 'center'
	},
	headerText: {
		color: 'white',
		alignSelf: 'center',
		textAlign: 'center',
		fontSize: 15,
		fontWeight: '500'
	},
	barIcon: {
		justifyContent: 'center',
		alignItems: 'center',
		width: 40
	},
	button: {
		height: 45,
		width: 45, 
		justifyContent: 'center'
	},
	buttonText: {
		color: 'white',
		fontSize: 15,
		textAlign: 'center',
		alignSelf: 'center',
		paddingLeft: 10,
		paddingRight: 10
	},
	cardContainer: {
		backgroundColor: 'rgba(0,0,0,0.3)',
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		height: 30,
		width: 90,
		borderRadius: 15,
		paddingLeft: 10,
		paddingRight: 10,
		marginLeft: 10
	},
	cardText: {
		color: 'white',
		fontSize: 13,
		paddingLeft: 5
	},
	shopIcon: {
		width: 17
	},
	icon: {
		height: 20
	},
	DWMContainer: {
		marginTop: 15,
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center'
	},
	DWMCell: {
		height: 30,
		flexGrow: 1
	},
	DWMSelectedCell: {
		flexGrow: 1,
		height: 30,
		borderBottomWidth: 1,
		borderBottomColor: 'white'
	},
	DWMSelectedText: {
		textAlign: 'center',
		color: 'white',
		fontWeight: '500'
	},
	DWMText: {
		textAlign: 'center',
		color: 'white'
	}
})
