import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
	Text,
	Image,
	View,
	TouchableHighlight,
	Dimensions
} from 'react-native'
import { connect } from 'react-redux'
import  { styles } from './styles'


class HeaderBar extends Component {
	static propTypes = {
		barType: PropTypes.string,
		title: PropTypes.string,
		rightTitle: PropTypes.string,
		leftTitle: PropTypes.string,
		rightCallback: PropTypes.func,
		leftCallback: PropTypes.func,
		specialCallback: PropTypes.func
	}

	constructor(props) {
		super(props)
	}

	render() {
		switch (this.props.barType) {
		case 'fitness':
			return <FitnessHeader {...this.props}/>
		case 'shop':
			return <ShopHeader {...this.props}/>
		case 'twoButton':
			return <TwoButton {...this.props}/>
		case 'socialBoard':
			return <SocialBoardHeader {...this.props}/>
		default:
			return <SimpleHeader {...this.props}/>
		}
	}
}

const SimpleHeader = (props) => {
	return (
		<View style={[styles.headerContainer, { justifyContent: 'center' }]}>
			{(props.title) && <Text style={styles.headerText}>{props.title}</Text>}
		</View>
	)
}

const TwoButton = (props) => {
	return (
		<View style={[styles.headerContainer, { justifyContent: 'space-between' }]}>
			<TouchableHighlight
				style={[styles.button, {width: undefined}]}
				underlayColor={'rgba(255,255,255,0.3)'}
				onPress={props.leftCallback != null
					? () => props.leftCallback()
					: () => props.navigation.goBack()}
			>
				<Text style={styles.buttonText}>{props.leftTitle != null
					? props.leftTitle
					: 'Cancel'}
				</Text>
			</TouchableHighlight>
			{(props.title) && <Text style={styles.headerText}>{props.title}</Text>}
			<TouchableHighlight
				style={[styles.button, {width: undefined}]}
				underlayColor={'rgba(255,255,255,0.3)'}
				onPress={() => props.rightCallback()}
			>
				<Text style={styles.buttonText}>{props.rightTitle != null
					? props.rightTitle
					: 'Confirm'}
				</Text>
			</TouchableHighlight>
		</View>
	)
}

const ShopHeader = (props) => {
	return (
		<View style={[styles.headerContainer, { justifyContent: 'space-between' }]}>
			<TouchableHighlight
				style={{ width: Dimensions.get('window').width / 3 }}
				underlayColor='rgba(0,0,0,0)'
				onPress={() => props.specialCallback()}
			>
				<View style={styles.cardContainer}>
					
					<Text style={styles.cardText}>64034</Text>
				</View>
			</TouchableHighlight>
			<Text style={[styles.headerText, { alignSelf: 'center' }]}>Rewards</Text>
			<View style={[styles.cardContainer, { width: Dimensions.get('window').width / 3, backgroundColor: 'rgba(0,0,0,0)' }]}/>
		</View>
	)
}

const SocialBoardHeader = (props) => {
	return (
		<View style={[styles.headerContainer, { justifyContent: 'space-between' }]}>
			<TouchableHighlight
				style={[styles.barIcon, { width: Dimensions.get('window').width / 3}]}
				underlayColor='rgba(0,0,0,0)'
				onPress={()=>props.specialCallback()}
			>
				
			</TouchableHighlight>
			<Text style={[styles.headerText, { alignSelf: 'center' }]}>{props.title}</Text>
			<View style={[styles.barIcon, { width: Dimensions.get('window').width / 3}]}/>
		</View>
	)
}

const FitnessHeader = (props) => {
	return (
		<View style={styles.headerContainer}>
			<TouchableHighlight
				style={styles.barIcon}
				underlayColor='rgba(0,0,0,0)'
				onPress={()=>props.navigation.navigate('settings')}
			>
				
			</TouchableHighlight>
			<View style={styles.DWMContainer}>
				<SliderTab props={props} title={ 'Today' }/>
				<SliderTab props={props} title={ 'Week' }/>
				<SliderTab props={props} title={ 'Month' }/>
			</View>
			<TouchableHighlight
				style={styles.barIcon}
				underlayColor='rgba(0,0,0,0)'
				onPress={()=>props.navigation.navigate('goals')}
			>
				
			</TouchableHighlight>
		</View>
	)
}

const SliderTab = ({ props, title }) => {
	let range = props.fitnessRange
	return (
		<TouchableHighlight
			underlayColor='rgba(0,0,0,0)'
			style={range == title ? styles.DWMSelectedCell : styles.DWMCell}
			onPress={() => props.setFitnessRange(title)}
		>
			<Text style={range == title ? styles.DWMSelectedText : styles.DWMText}
			>{title}</Text>
		</TouchableHighlight>
	)
}

function mapStateToProps(state) {
	return {
		fitnessRange: state.fitnessRange,
		points: state.points
	}
}

export default connect(mapStateToProps)(HeaderBar)

