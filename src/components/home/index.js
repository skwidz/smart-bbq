import React, { Component } from 'react'
import { Image, View, StatusBar, Linking } from 'react-native'

import { Container, Button, H3, Text, Header, Title, Body, Left, Right, Icon } from 'native-base'
import LinearGradient from 'react-native-linear-gradient'
import HeaderBar from '../headerbar'
import styles from './styles'
import * as colors from '../../constants/Colors'
import CircleProgress from '../../components/circleProgress'

class Home extends Component {
	constructor(props) {
		super(props)

		this.state = {
			fill: 46
		}
	}

	render() {
		const data = [
			{
				name: 'beef',
				type: 'timer',
				fill: this.state.fill
			},
			{
				name: 'beef',
				type: 'temp',
				fill: this.state.fill
			},
			{
				name: 'beef',
				type: 'timer',
				fill: this.state.fill
			}
		]
  	return (
  		<View style={{ flex: 1 }}>
				<LinearGradient colors={colors.GRADIENT}
					style={{ flex: 1 }}
				>
    			<HeaderBar
  					title='smart-bbq'
						barType='twoButton'
						rightTitle='Help'
						leftTitle='Settings'
  					leftCallback={()=> this.props.navigation.navigate('Settings')}
						{...this.props}
  				/>
					<View style={{ flex: 1 }}>
						<CircleProgress
							{...this.props}
							data={data}
							
						/>
						
					</View>
					<Button
						onPress={()=>this.setState({fill:Math.round(Math.random() * 100)})}
					><Text>HERE WEE ARE</Text>
					</Button>
				</LinearGradient>
			</View>

  	)
	}
}

export default Home
