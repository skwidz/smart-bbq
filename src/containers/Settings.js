import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
	Text,
	View
} from 'react-native'
import HeaderBar from '../components/headerbar'
import RowButton from '../components/rowbutton'
import LinearGradient from 'react-native-linear-gradient'
import { GRADIENT } from '../constants/Colors'

class ContainerName extends Component {
	constructor(props) {
		super(props)
		this.state = {}
	}

	render() {
		return (
			<View style={{flex:1}}>
				<LinearGradient style={{flex:1}} colors={ GRADIENT }>
					<HeaderBar
						{...this.props}
						title='Settings'
						barType='twoButton'
					/>
					<RowButton
						title={'Temp Units'}
						callback={()=>alert('change units')}
					/>
					<RowButton
						title={'Notification Settings'}
						callback={()=>alert('notifications screen')}
					/>
				</LinearGradient>
			</View>
		)
	}
}

export default connect()(ContainerName)
