import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
	Text,
	View
} from 'react-native'
import HeaderBar from '../components/headerbar'
import LinearGradient from 'react-native-linear-gradient'
import { GRADIENT } from '../constants/Colors'

class ContainerName extends Component {
	constructor(props) {
		super(props)
		this.state = {}
	}

	render() {
		return (
			<View style={{flex:1}}>
				<LinearGradient style={{ flex:1 }} colors={ GRADIENT }>
					<HeaderBar
						title='title'
					/>
				</LinearGradient>
			</View>
		)
	}
}

export default connect()(ContainerName)
